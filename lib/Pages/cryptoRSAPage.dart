import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobile/utils/RSAController.dart';
import 'package:open_file/open_file.dart';
import 'dart:convert';

import 'package:path_provider/path_provider.dart';

class CryptoRSAPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CryptoRSAPageState();
}

class CryptoRSAPageState extends State<CryptoRSAPage> {
  final rsaController = RSAController();
  var publicKeyFileName = '';
  var publicKeyFilePath = '';
  var privateKeyFileName = '';
  var privateKeyFilePath = '';
  var ambientFileName = '';
  var ambientFilePath = '';
  var encryptedFileName = '';
  var encryptedFilePath = '';
  var signatureFileName = '';
  var signatureFilePath = '';

  String msgAlert = '';
  String textFieldValue = '';

  void generateKeyPairFiles() async {
    try {
      var timestamp = DateTime.now().millisecondsSinceEpoch;
      Directory dir = await getExternalStorageDirectory();
      rsaController.generateRSAkeyPair(rsaController.getSecureRandom(),
          bitLength: 1024);

      setState(() {
        privateKeyFileName = 'private_key_$timestamp.txt';
        publicKeyFileName = 'public_key_$timestamp.txt';
        publicKeyFilePath = '${dir.path}/$publicKeyFileName.txt';
        privateKeyFilePath = '${dir.path}/$privateKeyFileName.txt';
      });

      await File('${dir.path}/$privateKeyFileName.txt').writeAsString(
          rsaController.encodePrivateKeyToPem(rsaController.privateKey));
      await File('${dir.path}/$publicKeyFileName.txt').writeAsString(
          rsaController.encodePublicKeyToPem(rsaController.publicKey));

      setState(() {
        msgAlert = 'Keys pair has been created!';
      });
    } catch (error) {
      setState(() {
        msgAlert = 'Error! ${error.toString()}';
      });
    }
  }

  void encryptRSA() async {
    try {
      var dir = await getExternalStorageDirectory();

      await File('${dir.path}/encrypted_$ambientFileName').writeAsBytes([]);

      File(ambientFilePath).openRead().forEach((element) async {
        await File('${dir.path}/encrypted_$ambientFileName').writeAsBytes(
            rsaController.rsaEncrypt(rsaController.publicKey, element));
      });
      setState(() {
        encryptedFilePath = '${dir.path}/encrypted_$ambientFileName';
        encryptedFileName = 'encrypted_$ambientFileName';
      });
    } catch (error) {
      setState(() {
        msgAlert = error.toString();
        textFieldValue = error.toString();
        clearMsg();
      });
    }
  }

  void decriptRSA() async {
    try {
      var decrypted = '';
      await File(encryptedFilePath).openRead().forEach((element) {
        var decrypt =
            rsaController.rsaDecrypt(rsaController.privateKey, element);
        decrypted += Utf8Decoder().convert(decrypt);
      });
      setState(() {
        textFieldValue = decrypted;
      });
    } catch (error) {
      setState(() {
        msgAlert = error.toString();
        textFieldValue = "INVALID PRIVATE KEY!!!";
        clearMsg();
      });
    }
  }

  void createSignatureRSA() async {
    try {
      var dir = await getExternalStorageDirectory();

      await File('${dir.path}/signature_$ambientFileName').writeAsBytes([]);

      String plainText = await File(ambientFilePath).readAsString();
      String signature =
          rsaController.sign(plainText, rsaController.privateKey);
      await File('${dir.path}/signature_$ambientFileName')
          .writeAsString(signature);

      print(signature);

      setState(() {
        signatureFilePath = '${dir.path}/signature_$ambientFileName';
        signatureFileName = 'signature_$ambientFileName';
      });
    } catch (error) {
      setState(() {
        msgAlert = error.toString();
        textFieldValue = error.toString();
        clearMsg();
      });
    }
  }

  void verifySignatureRSA() async {
    try {
      String verified = await File(signatureFilePath).readAsString();
      String ambient = await File(ambientFilePath).readAsString();
      bool isVerified =
          rsaController.verify(verified, ambient, rsaController.publicKey);

      setState(() {
        textFieldValue = 'Подпись не верифицирована!';
        if (isVerified) {
          textFieldValue = 'Подпись верифицирована!';
        }
      });
    } on Exception catch (error) {
      msgAlert = error.toString();
      textFieldValue = error.toString();
      clearMsg();
    }
  }

  void clearMsg() async {
    await Future.delayed(Duration(seconds: 10));
    setState(() {
      msgAlert = '';
    });
  }

  @override
  Widget build(BuildContext context) =>
      ListView(padding: EdgeInsets.all(20), children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 12),
          child: Text(
            'ЛР №6. RSA',
            textScaleFactor: 2,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(msgAlert),
              Divider(),
              Text(
                'Keys',
                textAlign: TextAlign.center,
              ),
              ElevatedButton(
                  onPressed: generateKeyPairFiles,
                  child: Text('Create a new key pair')),
              Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text('public key: $publicKeyFileName')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        try {
                          File file = await FilePicker.getFile();
                          var pemString = await file.readAsString();
                          rsaController.publicKey =
                              rsaController.parsePublicKeyFromPem(pemString);
                          setState(() {
                            publicKeyFileName =
                                file.path.split('/').last.toString();
                            publicKeyFilePath = file.path;
                          });
                        } catch (error) {
                          print(error);
                        }
                      },
                      child: Text('choose')),
                  ElevatedButton(
                      onPressed: () {
                        try {
                          OpenFile.open(publicKeyFilePath);
                        } catch (error) {}
                      },
                      child: Text('open')),
                  ElevatedButton(
                      onPressed: () async {
                        String text =
                            await File(publicKeyFilePath).readAsString();
                        setState(() {
                          textFieldValue = text;
                        });
                      },
                      child: Text('print')),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text('private key: $privateKeyFileName')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        try {
                          File file = await FilePicker.getFile();
                          var pemString = await file.readAsString();
                          rsaController.privateKey =
                              rsaController.parsePrivateKeyFromPem(pemString);
                          setState(() {
                            privateKeyFileName =
                                file.path.split('/').last.toString();
                            privateKeyFilePath = file.path;
                          });
                        } catch (error) {}
                      },
                      child: Text('choose')),
                  ElevatedButton(
                      onPressed: () async {
                        try {
                          var dir = await getExternalStorageDirectory();
                          OpenFile.open('${dir.path}/$privateKeyFileName');
                        } catch (error) {}
                      },
                      child: Text('open')),
                  ElevatedButton(
                      onPressed: () async {
                        String text =
                            await File(privateKeyFilePath).readAsString();
                        setState(() {
                          textFieldValue = text;
                        });
                      },
                      child: Text('print')),
                ],
              ),
              Divider(),
              Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text('ambient file: $ambientFileName')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        try {
                          File file = await FilePicker.getFile();
                          setState(() {
                            ambientFilePath = file.path;
                            ambientFileName =
                                file.path.split('/').last.toString();
                          });
                        } catch (error) {}
                      },
                      child: Text('choose')),
                  ElevatedButton(
                      onPressed: () {
                        try {
                          OpenFile.open(ambientFilePath);
                        } catch (error) {}
                      },
                      child: Text('open')),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text('encrypted file: $encryptedFileName')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        try {
                          File file = await FilePicker.getFile();
                          setState(() {
                            encryptedFilePath = file.path;
                            encryptedFileName =
                                file.path.split('/').last.toString();
                          });
                        } catch (error) {}
                      },
                      child: Text('choose')),
                  ElevatedButton(
                      onPressed: () {
                        try {
                          OpenFile.open(encryptedFilePath);
                        } catch (error) {}
                      },
                      child: Text('open')),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text('signature file: $signatureFileName')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        try {
                          File file = await FilePicker.getFile();
                          setState(() {
                            signatureFilePath = file.path;
                            signatureFileName =
                                file.path.split('/').last.toString();
                          });
                        } catch (error) {}
                      },
                      child: Text('choose')),
                  ElevatedButton(
                      onPressed: () {
                        try {
                          OpenFile.open(signatureFilePath);
                        } catch (error) {}
                      },
                      child: Text('open')),
                ],
              ),
              Divider(),
              Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text('first step actions')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: encryptRSA, child: Text('зашифровать')),
                  ElevatedButton(
                      onPressed: createSignatureRSA, child: Text('подписать')),
                ],
              ),
              Divider(),
              Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Text('second step actions')),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: decriptRSA, child: Text('расшифровать')),
                  ElevatedButton(
                      onPressed: verifySignatureRSA,
                      child: Text('проверить подпись')),
                ],
              ),
              Divider(),
              Container(
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Text(textFieldValue
                    // decoration: InputDecoration(
                    //   // hintText: (keyAES.length == 0) ? 'Введите ключ' : keyAES,
                    //   labelStyle: TextStyle(fontSize: 24, color: Colors.blueAccent),
                    //   border: OutlineInputBorder(),
                    // ),
                    ),
              ),
            ],
          ),
        ),
      ]);
}
