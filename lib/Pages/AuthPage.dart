import 'package:flutter/material.dart';

import 'package:flutter_mobile/Pages/loginVkPage.dart';

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AuthPageState();
}

class AuthPageState extends State<AuthPage> {
  final loginVkView = LoginVK();

  @override
  Widget build(BuildContext context) {
    final String vkIcnPath = 'assets/images/vk.png';
    AssetImage vkIcnImage = AssetImage(vkIcnPath);
    return Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        children: [
          Text(
            'ЛР №4. oAuth2',
            textScaleFactor: 2,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Expanded(
              child: Center(
            child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => loginVkView));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('Авторизация через ',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    new Image(image: vkIcnImage, fit: BoxFit.cover, height: 25)
                  ],
                )),
          )),
        ],
      ),
    );
  }
}
